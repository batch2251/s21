// EASY WAY WITHOUT DIRECTION

// let users = ['Dwayne Johnson', 'Steve Austin', 'Kurt Angle', 'Dave Bautista']

// console.log('Original Array:');
// console.log(users);

// // users.length = users.length+1
// // users[4] = "John Cena";
// users.push('John Cena')
// console.log(users);


// console.log(users[2]);
// console.log(users[4]);

// // users.length = users.length-1
// // console.log(users);
// users.pop()
// console.log(users);


// users[3] = 'Triple H'
// console.log(users);

// console.log([]);

// console.log(true);

// console.log(users[20]);

// =====================================================================
// ===== FOLLOWING THE HARD WAY INSTRUCTION =================

let users = ['Dwayne Johnson', 'Steve Austin', 'Kurt Angle', 'Dave Bautista']

console.log('Original Array:');
console.log(users);

/*1. Create a function which is able to receive a single argument and add the input at the end of the users array.
        -function should be able to receive a single argument.
        -add the input data at the end of the array.
        -The function should not be able to return data.
        -invoke and add an argument to be passed in the function.
        -log the users array in the console.*/

function addAtEnd (Arr){
	let users = ['Dwayne Johnson', 'Steve Austin', 'Kurt Angle', 'Dave Bautista']
	users.push(Arr);
console.log(users);
}
addAtEnd('John Cena');
// ===============================================================

/*2. Create function which is able to receive an index number as a single argument return the item accessed by its index.
        -function should be able to receive a single argument.
        -return the item accessed by the index.
        -Create a global variable called outside of the function called itemFound and store the value returned by the function in it.
        -log the itemFound variable in the console.*/

var itemFound = ['Dwayne Johnson', 'Steve Austin', 'Kurt Angle', 'Dave Bautista', 'John Cena']
function newItem (i) {
	console.log(itemFound[i]);
}
newItem (2);
newItem (4);
// ===============================================================

/*3. Create function which is able to delete the last item in the array and return the deleted item.
        -Create a function scoped variable to store the last item in the users array.
        -Shorten the length of the array by at least 1 to delete the last item.
        -return the last item in the array which was stored in the variable.*/

function deleteLastItem () {
	let newItems = itemFound
	newItems.length = newItems.length-1
console.log(newItems);
}
deleteLastItem();
// ===============================================================

/*4. Create function which is able to update a specific item in the array by its index.
        -Function should be able to receive 2 arguments, the update and the index number.
        -First, access and locate the item by its index then re-assign the item with the update.
        -This function should not have a return.
        -Invoke the function and add the update and index number as arguments.
        -log the users array in the console.*/

function arrayByIndex (a, b) {
	let newItems = itemFound
	newItems[a] = b;
console.log(newItems);
}
arrayByIndex(3, 'Triple H');
// ===============================================================

/*5. Create function which is able to delete all items in the array.
        -You can modify/set the length of the array.
        -The function should not return anything.*/

function deleteAll() {
	let newItems = itemFound
	newItems.length=0;
console.log(newItems);
}
deleteAll();
// ===============================================================

/*6. Create a function which is able to check if the array is empty.
        -Add an if statement to check if the length of the users array is greater than 0.
            -If it is, return false.
        -Else, return true.
        -Create a global variable called outside of the function  called isUsersEmpty and store the returned value from the function.
        -log the isUsersEmpty variable in the console.*/

var itemFound2 = ['Dwayne Johnson', 'Steve Austin', 'Kurt Angle', 'Dave Bautista', 'John Cena']
var isUsersEmpty = null
function isArrayNotEmpty () {
	let newItems = itemFound2
	if(newItems.length > 0) {
		isUsersEmpty = true;	
	} else {
		isUsersEmpty = false
	} 
console.log(isUsersEmpty);
}
isArrayNotEmpty();